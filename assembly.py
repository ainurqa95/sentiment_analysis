import os
import matplotlib.pyplot as plt
import pandas as pd


def isNaN(num):
    return num != num

categories = ['animals',
              'auto', 'deti', 'kino', 'knigi',
              'products', 'krasota_zdorovie', 'technika', 'tech_operatory', 'tourism',  'tv',
              'autoshkola', 'banks', 'kafe', 'strahovie', 'shop_centr', 'razvlek_centr',
              'alkogol','igri', 'kino2', 'knigi22', 'products2', 'sayti', 'kino2', 'kino3', 'kliniki', 'himiya', 'kanstovari', 'khobbi', 'phone',
              ]
# categories =['tv']
# dfResult = pd.read_csv('parse_content/result_positive.csv')
dfResult = pd.DataFrame()
k = 0
for cat in categories:
    for type in ['positive', 'negative']:
        for j in range(0, 40):
            directory = "parse_content/" + str(cat) + "/" + str(type) + str(j)
            print(directory)
            if (os.path.isdir(directory)):
                for l in range(0, 40):
                    filePath = directory + '/page_' + str(l) + '.csv'
                    print(filePath)
                    if (os.path.isfile(filePath)):
                        df = pd.read_csv(filePath)
                        dfResult = pd.concat([dfResult, df], ignore_index=False)
                        k = k + 1

# print(dfResult['tag '])
dfResult.drop_duplicates(subset=['title'], keep=False)

dfResult.columns = ['id', 'category', 'title', 'text', 'rating_review', 'rating_product', 'tag']

# print(dfResult['tag'])

print(dfResult.keys())
filtToSave = 'parse_content/reviews_assembly.csv'
dfResult.set_index('id')
dfResult.dropna()
cont = 0
dfResult = dfResult.loc[dfResult['text'] == dfResult['text']] # Nan check
for i, row in dfResult.iterrows(): # меняем разметку
    if isNaN(row['tag']) or isNaN(row['text']):
        # dfResult = dfResult[dfResult.index != i]
        cont = cont + 1
        print(row)
print(cont)

dfOne = dfResult.loc[dfResult['rating_review'] == 1]
dfTwo = dfResult.loc[dfResult['rating_review'] == 2]
dfThree = dfResult.loc[dfResult['rating_review'] == 3]
dfFour = dfResult.loc[dfResult['rating_review'] == 4]
dfFive = dfResult.loc[dfResult['rating_review'] == 5]
dfThree = dfThree.sample(frac=0.8)
dfResult = pd.concat([dfOne, dfTwo, dfThree, dfFour, dfFive])

dfResult.loc[dfResult['rating_review'] == 3, 'tag'] = -1
dfResult.loc[dfResult['rating_review'] == 4, 'tag'] = 1

dfNegative = dfResult.loc[dfResult['tag'] == -1]
dfPositive = dfResult.loc[dfResult['tag'] == 1]
dfPositive = dfPositive.sample(frac=0.45)
dfResult = pd.concat([dfNegative, dfPositive])

dfResult['rating_review'].value_counts().plot(kind="bar", rot=0)
plt.savefig('result_images/data_counts.png')
plt.close()
print(dfResult.count())
dfResult['tag'].value_counts().plot(kind="bar", rot=0)
plt.savefig('result_images/data_counts2.png')
plt.close()

dfResult.set_index('id').to_csv(filtToSave)
