import os
import multiprocessing
import re
import sqlite3

import gensim
import pandas as pd
from gensim.models import Word2Vec, FastText

from gensim.models.wrappers import Wordrank

import logging

def preprocess_text(text):
    text = text.lower().replace("ё", "е")
    text = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', '', text)
    text = re.sub('@[^\s]+', '', text)
    text = re.sub('[^a-zA-Zа-яА-Я1-9]+', ' ', text)
    text = re.sub(' +', ' ', text)
    return text.strip()


# df3 = pd.read_csv('parse_content/positive_negative_precomputed2.csv')
# text = ''
# for i, row in df3.iterrows(): # меняем разметку
#     text += row['title'] + '\n'
# print(text)
# with open("data_w2v/w2v_train.txt", "w") as text_file:
#     text_file.write(text)

# Открываем SQLite базу данных
# conn = sqlite3.connect('data_w2v/mysql2sqlite/mysqlite3.db')
# c = conn.cursor()
#
# with open('data_w2v/tweets.txt', 'w', encoding='utf-8') as f:
#     # Считываем тексты твитов
#     for row in c.execute('SELECT ttext FROM sentiment'):
#         if row[0]:
#             tweet = preprocess_text(row[0])
#             # Записываем предобработанные твиты в файл
#             print(tweet, file=f)
# #
# os.sys.exit()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def train_model_based_type(type, model_name, corpus_file = 'data_w2v/w2v_train.txt'):
    if type == 'w2v':
        data = gensim.models.word2vec.LineSentence(corpus_file)
        model = Word2Vec(data, size=300, window=5, min_count=3, workers=multiprocessing.cpu_count())
    if type == 'fast_text':
        model = FastText(corpus_file=corpus_file, size=100, window=5, min_count=1, iter=10)
        from gensim.test.utils import get_tmpfile
        fname = get_tmpfile("/home/ainur/PycharmProjects/tonality/data_" + type + "/" + model_name + "_"+ type +".model")
        model.save(fname)
        return model
    if type == 'wordrank':
        model = Wordrank.train(wr_path='data_wordrank', corpus_file=corpus_file, out_name=type+'_'+model_name, size=300, window=5, symmetric=1, min_count=5, max_vocab_size=0,
              sgd_num=100, lrate=0.001, period=10, iter=90, epsilon=0.75, dump_period=10, reg=0, alpha=100, beta=99,
              loss="hinge", memory=4.0, np=1, cleanup_files=False, sorted_vocab=1, ensemble=0)

    model.save("data_" + type + "/" + model_name + "_"+ type +".model")
    return model

def load_model_based_type(type, model_name):
    if type == 'w2v':
        return Word2Vec.load("data_" + type + "/" + model_name + "_"+ type +".model")
    if type == 'fast_text':
        return FastText.load("data_" + type + "/" + model_name + "_"+ type +".model")
    if type == 'wordrank':
        return Wordrank.load("data_" + type + "/" + model_name + "_"+ type +".model")

type = "fast_text"
model_name = "tweet"
train_model_based_type(type, model_name, 'data_w2v/tweets.txt')
model = load_model_based_type(type, model_name)
# Долго обучается не запускать часто :)
# train_model_based_type("fast_text", "our_small")
# model = load_model_based_type("fast_text", "our_small")


print(model.most_similar(positive='хороший', topn=5))

from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
# % matplotlib inline


def tsne_plot(labels, tokens, classes, clusters, type):
    tsne_model = TSNE(perplexity=15, n_components=2, init='pca', n_iter=3500, random_state=33)
    new_values = tsne_model.fit_transform(tokens)

    x = []
    y = []

    for value in new_values:
        x.append(value[0])
        y.append(value[1])

    colors = cm.rainbow(np.linspace(0, 1, clusters))
    plt.figure(figsize=(16, 9))
    for i in range(len(x)):
        plt.scatter(x[i], y[i], c=colors[classes[i]], alpha=0.75)
        plt.annotate(labels[i], alpha=0.75, xy=(x[i], y[i]), xytext=(5, 2),
                     textcoords='offset points', ha='right', va='bottom', size=10)
    plt.grid(True)
    plt.show()
    plt.savefig('result_images/'+type+'.png')

labels = []
tokens = []
classes = []

samples = 15
for i, word in enumerate(['отрицание', 'продукт', 'нравиться', 'популярный']):#random.sample(list(model.wv.vocab), samples)):#['ссора', 'iphone', 'обидел', 'красивый', 'нравится', 'домашка']):#random.sample(list(model.wv.vocab), 10):
    tokens.append(model.wv[word])
    labels.append(word)
    classes.append(i)
    for similar_word, similarity in model.wv.most_similar(word, topn=20):
        tokens.append(model.wv[similar_word])
        labels.append(similar_word)
        classes.append(i)

tsne_plot(labels, tokens, classes, samples, model_name + '_' + type)

