import os
import re
import time
import uuid
from collections import Counter

from selenium import webdriver


def getTagBasedRating(rating):
    if int(rating) <= 2:
        return -1
    if int(rating) >= 3 and int(rating) <= 4:
        return 0
    if int(rating) == 5:
        return 1


def getDescription(driver):
    description = driver.find_element_by_class_name('reviewText')
    paragraphs = description.find_elements_by_css_selector('p')
    paragraphs = [x for x in paragraphs if x.text]
    lenP = len(paragraphs)
    print(lenP)
    if lenP >= 3:
        description = paragraphs[-3].text + " " + paragraphs[-2].text + " " + paragraphs[-1].text + " "
        words = description.split()
        wordCount = len(words)
        if wordCount < 30 and lenP >= 6:  # если маленький отзыв
            description = " " + paragraphs[-6].text + " " + paragraphs[-5].text + " " + paragraphs[
                -4].text + " " + description

    elif lenP == 2:
        description = paragraphs[-2].text + " " + paragraphs[-1].text
    else:
        description = description.text
    description = description.replace('\n', ' ')
    description = re.sub(r'[",?.!]+', ' ', description)
    return description

def get_negative_reviews (page, driver):
    driver.get(page)
    blocks = driver.find_elements_by_class_name('reviewTextSnippet')
    reviewUrls = []
    for elem in blocks:
        a = elem.find_element_by_css_selector('a')
        reviewUrl = a.get_attribute('href')
        reviewUrls.append(reviewUrl)
    reviewRatings = driver.find_elements_by_xpath('//meta[@itemprop="ratingValue"]')
    reviewRatingsAll = []
    for s in range(1, len(reviewRatings) - 3):
        reviewRatingsAll.append(reviewRatings[s].get_attribute('content'))
    delIndexes = []
    for j in range(0,len(reviewRatingsAll)):
        if int(reviewRatingsAll[j]) > 2 :
            delIndexes.append(j)
    reviewUrls = [x for i, x in enumerate(reviewUrls) if i not in delIndexes]
    return reviewUrls

option = webdriver.ChromeOptions()
chrome_prefs = {}
option.experimental_options["prefs"] = chrome_prefs
chrome_prefs["profile.default_content_settings"] = {"images": 2}
chrome_prefs["profile.managed_default_content_settings"] = {"images": 2}
# option.add_argument("--window-size=100,100")

genres = {

          'https://irecommend.ru/category/dekorativnaya-kosmetika?tid=887&page=1',
          }
k = 14
key = 'kolyaski'
# for value in genres:
for q in range(0,15):
    colums = "id,category,title,text,rating_review,rating_product,tag \n";
    driver = webdriver.Chrome(chrome_options=option)
    value = 'https://irecommend.ru/category/tovary-dlya-novorozhdennykh?tid=145819&page=' + str(q)
    driver.get(value)
    time.sleep(5)
    links_of_items = driver.find_elements_by_class_name('read-all-reviews-link')
    pages = []
    for link in links_of_items :
        pages.append(link.get_attribute('href'))
    driver.quit()
    pages = set(pages)
    pages = list(pages)
    print(pages)
    for i in range(0, len(pages)):
        resultFile = colums
        page = pages[i]
        print(page)
        driver = webdriver.Chrome(chrome_options=option)
        reviewUrls = get_negative_reviews(page,driver)
        j = 0
        for href in reviewUrls:

            print(href)
            driver.get(href)
            description = getDescription(driver)
            ratings = driver.find_elements_by_xpath('//meta[@itemprop="ratingValue"]')
            ratingReview = ratings[1].get_attribute('content')
            ratingProduct = ratings[0].get_attribute('content')
            tag = getTagBasedRating(ratingReview)
            resultFile = resultFile + str(uuid.uuid4()) + ',"' + str(key) + '","' + str(href) + '","' + str(
                description) + '","' + str(ratingReview) + '","' + ratingProduct + '","' + str(tag) + '"\n';

            directory = 'parse_content/'+str(key)+'/negative'+str(k)
            if not os.path.exists(directory):
                os.makedirs(directory)
            path = directory + '/page_' + str(i) + '.csv'
            f = open(path, "w+")
            f.write(resultFile)
            f.close()
            blocks = driver.find_elements_by_class_name('reviewTextSnippet')
            j = j + 1

        driver.quit()
    k = k + 1
