from sklearn.externals import joblib
from sklearn.neural_network import MLPClassifier

from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class MlpClassificator(BaseClassificator):

    def load_classificator(self):
        self._type = "MLP"
        self._classificator = MLPClassifier(solver='lbfgs', alpha=1e-8, hidden_layer_sizes=(6, 6), random_state=2)


