from tonality.tonality_classes.Classificators.CNNClassificator import CNNClassificator
from tonality.tonality_classes.Classificators.MlpClassificator import MlpClassificator
from tonality.tonality_classes.Classificators.RFClassificator import RFClassificator
from tonality.tonality_classes.Classificators.RNNClassificator import RNNClassificator
from tonality.tonality_classes.Classificators.SVCClassificator import SVCClassificator


class ClassificatorFactory:

    @staticmethod
    def get_classificator_based_type(type_classificator, type_vector, type_classification):
        if type_classificator == 'MLP':
            classificator = MlpClassificator("MLP", type_vector, type_classification)
        elif type_classificator == 'RF':
            classificator = RFClassificator("RF", type_vector, type_classification)
        elif type_classificator == 'SVC':
            classificator = SVCClassificator("SVC", type_vector, type_classification)

        elif type_classificator == 'CNN':
            classificator = CNNClassificator("CNN", type_vector, type_classification)
        elif type_classificator == 'RNN':
            classificator = RNNClassificator("RNN", type_vector, type_classification)
        else:
            classificator = SVCClassificator("SVC", type_vector, type_classification)
        classificator.get_saved_model()
        return classificator