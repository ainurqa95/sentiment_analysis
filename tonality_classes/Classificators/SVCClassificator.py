from sklearn.calibration import CalibratedClassifierCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC, SVC

from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class SVCClassificator(BaseClassificator):
    def load_classificator(self):
        self._type = "SVC"
        self._classificator = LinearSVC()
