from sklearn.externals import joblib
import matplotlib.pyplot as plt
from keras import backend as K


class BaseClassificator:

    def __init__(self, type, wv_type, classification_type):
        self._type = type
        self._classificator = ""
        self.wv_type = wv_type
        self.classification_type = classification_type
        self._save_model_path = "../trained_models/" + self.classification_type \
                                + '/' + self.wv_type + "_classificator" + self._type
        if self._type == 'CNN':
            self._save_model_path += '.hdf5'
        else:
            self._save_model_path += '.pkl'
    def load_classificator(self):
        pass


    def get_type(self):
        return self._type

    def save_model(self):
        joblib.dump(self._classificator, self._save_model_path)  # после обучения запихиваем обученный в файл

    def get_saved_model(self):
        self._classificator = joblib.load(self._save_model_path)

    def get_model(self):
        return self._classificator

    def fit(self, train_features, tags):
        self._classificator.fit(train_features,tags)

    def predict(self, test_data_features):
        return self._classificator.predict(test_data_features)

    def predict_proba(self, test_data_features):
        return self._classificator.predict_proba(test_data_features)

    def precision(self, y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    def recall(self, y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def f1(self, y_true, y_pred):
        def recall(y_true, y_pred):
            """Recall metric.

            Only computes a batch-wise average of recall.

            Computes the recall, a metric for multi-label classification of
            how many relevant items are selected.
            """
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
            recall = true_positives / (possible_positives + K.epsilon())
            return recall

        def precision(y_true, y_pred):
            """Precision metric.

            Only computes a batch-wise average of precision.

            Computes the precision, a metric for multi-label classification of
            how many selected items are relevant.
            """
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
            precision = true_positives / (predicted_positives + K.epsilon())
            return precision

        precision = precision(y_true, y_pred)
        recall = recall(y_true, y_pred)
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


    def plot_metrix(self, ax, x1, x2, title):
        ax.plot(range(1, len(x1) + 1), x1, label='train')
        ax.plot(range(1, len(x2) + 1), x2, label='val')
        ax.set_ylabel(title)
        ax.set_xlabel('Epoch')
        ax.legend()
        ax.margins(0)

    def plot_history(self, history, history_name):
        fig, axes = plt.subplots(ncols=2, nrows=2, figsize=(16, 9))
        ax1, ax2, ax3, ax4 = axes.ravel()
        self.plot_metrix(ax1, history.history['precision'], history.history['val_precision'], 'Precision')
        self.plot_metrix(ax2, history.history['recall'], history.history['val_recall'], 'Recall')
        self.plot_metrix(ax3, history.history['f1'], history.history['val_f1'], "$F_1$")
        self.plot_metrix(ax4, history.history['loss'], history.history['val_loss'], 'Loss')
        plt.show()
        plt.savefig(history_name)
        plt.close()