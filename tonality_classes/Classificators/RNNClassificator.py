from keras import Model
from keras.engine.saving import load_model
from keras import backend as K
from keras.layers import Dense, concatenate, Activation, Dropout, Bidirectional, LSTM, MaxPooling1D, Flatten
from keras.models import Model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class RNNClassificator(BaseClassificator):


    def load_classificator_nn(self, SENTENCE_LENGTH, NUM, DIM, embedding_matrix):
        self._type = "RNN"

        from keras.layers import Input
        from keras.layers.embeddings import Embedding
        #
        tweet_input = Input(shape=(SENTENCE_LENGTH,), dtype='int32')
        tweet_encoder = Embedding(NUM, DIM, input_length=SENTENCE_LENGTH,
                                  weights=[embedding_matrix], trainable=False)(tweet_input)

        l_lstm = Bidirectional(LSTM(80, dropout=0.2, recurrent_dropout=0.2))(tweet_encoder)
        preds = Dense(1, activation='sigmoid')(l_lstm)
        model = Model(tweet_input, preds)
        model.compile(loss='binary_crossentropy',optimizer='adam',   metrics=[self.precision, self.recall, self.f1])
        self._classificator = model

    def fit(self, train_features, tags):
        checkpoint = ModelCheckpoint("../models/rnn/"+ self.classification_type +"/"+ self.wv_type+"-rnn-frozen-embeddings-{epoch:02d}-{val_f1:.2f}.hdf5",
                                     monitor='val_f1', save_best_only=True, mode='max', period=1)
        return self._classificator.fit(train_features, tags, batch_size=32, epochs=8, validation_split=0.25, callbacks=[checkpoint])

    def save_model(self):
        self._classificator.save(self._save_model_path)
        # model.save_weights('models/cnn/cnnmodel_1.hdf5')

    def over_train_model(self, train_features, tags, weights_params):
        from keras import optimizers
        self.get_saved_model()
        self._classificator.load_weights("../models/rnn/"+ self.classification_type +"/"+ self.wv_type+"-rnn-frozen-embeddings-"+ weights_params+".hdf5")
        self._classificator.layers[1].trainable = True
        adam = optimizers.Adam(lr=0.0001)
        self._classificator.compile(loss='binary_crossentropy', optimizer=adam, metrics=[self.precision, self.recall, self.f1])
        self._classificator.summary()

        checkpoint = ModelCheckpoint("../models/rnn/"+ self.classification_type +"/"+ self.wv_type+"-rnn-trainable-{epoch:02d}-{val_f1:.2f}.hdf5", monitor='val_f1',
                                     save_best_only=True, mode='max', period=1)

        return self._classificator.fit(train_features, tags, batch_size=32, epochs=5, validation_split=0.25, callbacks=[checkpoint])


    def get_saved_model(self):
        self._classificator = load_model(self._save_model_path, custom_objects={'precision' : self.precision , 'recall' : self.recall, 'f1' : self.f1})

    def precision(self, y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    def recall(self, y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def f1(self, y_true, y_pred):
        def recall(y_true, y_pred):
            """Recall metric.

            Only computes a batch-wise average of recall.

            Computes the recall, a metric for multi-label classification of
            how many relevant items are selected.
            """
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
            recall = true_positives / (possible_positives + K.epsilon())
            return recall

        def precision(y_true, y_pred):
            """Precision metric.

            Only computes a batch-wise average of precision.

            Computes the precision, a metric for multi-label classification of
            how many selected items are relevant.
            """
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
            precision = true_positives / (predicted_positives + K.epsilon())
            return precision

        precision = precision(y_true, y_pred)
        recall = recall(y_true, y_pred)
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

