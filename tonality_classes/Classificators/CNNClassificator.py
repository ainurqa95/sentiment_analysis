from keras import Model
from keras.engine.saving import load_model
from keras import backend as K
from keras.layers import Dense, concatenate, Activation, Dropout, Bidirectional, LSTM, MaxPooling1D, Flatten
from keras.models import Model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class CNNClassificator(BaseClassificator):


    def load_classificator_nn(self, SENTENCE_LENGTH, NUM, DIM, embedding_matrix):
        self._type = "CNN"

        from keras.layers import Input
        from keras.layers.embeddings import Embedding
        #
        tweet_input = Input(shape=(SENTENCE_LENGTH,), dtype='int32')
        tweet_encoder = Embedding(NUM, DIM, input_length=SENTENCE_LENGTH,
                                  weights=[embedding_matrix], trainable=False)(tweet_input)
        # CNN
        branches = []
        x = Dropout(0.2)(tweet_encoder)

        for size, filters_count in [(2, 10), (3, 10), (4, 10), (5, 10)]:
            for i in range(filters_count):
                branch = Conv1D(filters=1, kernel_size=size, padding='valid', activation='relu')(x)
                branch = GlobalMaxPooling1D()(branch)
                branches.append(branch)

        x = concatenate(branches, axis=1)
        x = Dropout(0.2)(x)
        x = Dense(30, activation='relu')(x)
        dense = 3 if self.classification_type == 'news' else 1
        x = Dense(dense)(x)
        output = Activation('sigmoid')(x)
        model = Model(inputs=[tweet_input], outputs=[output])
        crossentropy = 'sparse_categorical_crossentropy' if self.classification_type == 'news' else 'binary_crossentropy'
        model.compile(loss=crossentropy , optimizer='adam', metrics=[self.precision, self.recall, self.f1])
        model.summary()
        self._classificator = model

    def fit(self, train_features, tags):
        checkpoint = ModelCheckpoint("../models/cnn/"+ self.classification_type +"/"+ self.wv_type+"-cnn-frozen-embeddings-{epoch:02d}-{val_f1:.2f}.hdf5",
                                     monitor='val_f1', save_best_only=True, mode='max', period=1)
        return self._classificator.fit(train_features, tags, batch_size=32, epochs=10, validation_split=0.25, callbacks=[checkpoint])

    def save_model(self):
        self._classificator.save(self._save_model_path)
        # model.save_weights('models/cnn/cnnmodel_1.hdf5')

    def over_train_model(self, train_features, tags, weights_params):
        from keras import optimizers
        self.get_saved_model()
        self._classificator.load_weights("../models/cnn/"+ self.classification_type +"/"+ self.wv_type+"-cnn-frozen-embeddings-"+ weights_params+".hdf5")
        self._classificator.layers[1].trainable = True
        adam = optimizers.Adam(lr=0.0001)
        self._classificator.compile(loss='binary_crossentropy', optimizer=adam, metrics=[self.precision, self.recall, self.f1])
        self._classificator.summary()

        checkpoint = ModelCheckpoint("../models/cnn/"+ self.classification_type +"/"+ self.wv_type+"-cnn-trainable-{epoch:02d}-{val_f1:.2f}.hdf5", monitor='val_f1',
                                     save_best_only=True, mode='max', period=1)

        return self._classificator.fit(train_features, tags, batch_size=32, epochs=5, validation_split=0.25, callbacks=[checkpoint])


    def get_saved_model(self):
        self._classificator = load_model(self._save_model_path, custom_objects={'precision' : self.precision , 'recall' : self.recall, 'f1' : self.f1})




