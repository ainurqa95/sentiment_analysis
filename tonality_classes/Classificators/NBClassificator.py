from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier, PassiveAggressiveClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB, ComplementNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline

from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class NBClassificator(BaseClassificator):
    def load_classificator(self):
        self._type = "NB"
        self._classificator = KNeighborsClassifier() # 0.78 accuracy
        # self._classificator = PassiveAggressiveClassifier()
        # self._classificator = GridSearchCV(text_clf, tuned_parameters, cv=10, scoring=score)()