from sklearn.ensemble import RandomForestClassifier

from tonality.tonality_classes.Classificators.BaseClassificator import BaseClassificator


class RFClassificator(BaseClassificator):
    def load_classificator(self):
        self._type = "RF"
        self._classificator = RandomForestClassifier(n_estimators=25)
