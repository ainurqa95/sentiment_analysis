import os
import re

import numpy as np
import nltk
import pandas as pd
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

from tonality.tonality_classes.ClassificationTypes.BaseClassification import BaseClassification
import matplotlib.pyplot as plt


class TweetClassification(BaseClassification):

    def w2v_tokenize_text_not_taged(self, wv, item_of_dataframe):
        tokens = []
        text = item_of_dataframe['text']
        for sent in nltk.sent_tokenize(text, language='russian'):
            for word in nltk.word_tokenize(sent, language='russian'):
                match = re.search(r"[а-яА-Яё-]+", word)
                if match == None:
                    continue
                word = word.lower()

                if word == 'не':
                    word = 'отрицание'
                if word in stopwords.words('russian'):  # deete stop words
                    continue
                # parse_result = morph.parse(word)[0]
                # word = parse_result.normal_form
                if word not in wv.vocab:
                    continue
                tokens.append(word)
        if len(tokens) < 3:
            self.arrayOfEmptyColumns.append(item_of_dataframe['id'])
        return tokens

    def preprocess_text(self, text):
        text = text.lower().replace("ё", "е")
        text = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', '', text)
        text = re.sub('@[^\s]+', '', text)
        text = re.sub('[^a-zA-Zа-яА-Я1-9]+', ' ', text)
        text = re.sub(' +', ' ', text)
        return text.strip()

    def prepare_data(self):
        self.type_classification = "tweet"
        self.to_preprocess_path = '../parse_content/positive_negative.csv'
        taged_name = "taged" if self.taged else "not_taged"
        self.train_data_preprocessed_path = '../parse_content/tweet_precomputed' + '_' + taged_name + '.csv'
        self.my_tags = [-1, 1]
        self.SENTENCE_LENGTH = 26
        self.NUM = 100000


    def preprocess(self):
        df3 = pd.read_csv(self.to_preprocess_path)
        df3['tag'].value_counts().plot(kind="bar", rot=0)
        plt.savefig('../result_images/' + self.type_classification + '_data_counts.png')
        plt.close()
        for index, row in df3.iterrows():
            df3.at[index, 'text'] = self.preprocess_text(row['text'])
        df3.to_csv(self.train_data_preprocessed_path)


    def preprocess_one(self, text, flag_taged=False):
        wv = self.wv.get_wv()
        type = self.wv.get_type()
        row = {'text' : text}
        if (type == 'w2v_news'):
            taged_tokens, not_taged = self.w2v_tokenize_text(wv, row)
            if flag_taged:
                return taged_tokens
            else:
                return not_taged
        else:
            return self.preprocess_text(row['text'])
            # return self.w2v_tokenize_text_not_taged(wv, row)