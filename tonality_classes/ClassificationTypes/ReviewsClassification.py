import os
import re
from collections import Counter

import numpy as np
import nltk
import pandas as pd
from keras.layers import Conv1D, GlobalMaxPooling1D
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from keras import backend as K
from keras.layers import Dense, concatenate, Activation, Dropout, Bidirectional, LSTM, MaxPooling1D, Flatten
from keras.models import Model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from tonality.tonality_classes.ClassificationTypes.BaseClassification import BaseClassification
import matplotlib.pyplot as plt

from tonality.tonality_classes.Classificators import CNNClassificator
from tonality.tonality_classes.Classificators.MlpClassificator import MlpClassificator
from tonality.tonality_classes.Classificators.RFClassificator import RFClassificator
from tonality.tonality_classes.Classificators.SVCClassificator import SVCClassificator


class ReviewsClassification(BaseClassification):

    def prepare_data(self):
        self.type_classification = "reviews"
        self.to_preprocess_path = '../parse_content/reviews_assembly.csv'
        taged_name = "taged" if self.taged else "not_taged"
        self.train_data_preprocessed_path = '../parse_content/reviews_precomputed' + '_' + self.wv.get_type() + '_' + taged_name + '.csv'
        self.my_tags = [-1, 1]
        self.SENTENCE_LENGTH = 70
        self.NUM = 15139



    def preprocess(self):
        df3 = pd.read_csv(self.to_preprocess_path)
        type = self.wv.get_type()
        wv = self.wv.get_wv()
        df3['tag'].value_counts().plot(kind="bar", rot=0)
        print(df3.count())
        plt.savefig('../result_images/' + self.type_classification + '/data_counts.png')
        plt.close()
        if (type == 'w2v_news'):
            for index, row in df3.iterrows():
                print(index)
                print(row)
                taged_tokens, not_taged = self.w2v_tokenize_text(wv, row)
                df3.at[index, 'text'] = taged_tokens
                df3.at[index, 'title'] = " " . join(not_taged)
        else:
            for index, row in df3.iterrows():
                taged_tokens = self.w2v_tokenize_text_not_taged(wv, row)
                # df3.at[index, 'text'] = taged_tokens
                # preprocessed =  self.preprocess_text(row['text'])
                # if(self.isNaN(preprocessed) or Counter(preprocessed.lower().split()) <= 5):
                #     print(row)
                df3.at[index, 'text'] = taged_tokens


        for item in self.arrayOfEmptyColumns:
            df3 = df3[df3.id != item]
        df3.to_csv(self.train_data_preprocessed_path)

    def preprocess_one(self, text, flag_taged=False):
        wv = self.wv.get_wv()
        type = self.wv.get_type()
        row = {'text' : text}
        if (type == 'w2v_news'):
            taged_tokens, not_taged = self.w2v_tokenize_text(wv, row)
            if flag_taged:
                return taged_tokens
            else:
                return not_taged
        else:
            # return self.preprocess_text(row['text'])
            return self.w2v_tokenize_text_not_taged(wv, row)




