import os
import pickle
import sys
from pathlib import Path
import time

import pymorphy2
import xlrd
from gensim.models import word2vec, FastText
import logging
from scipy.interpolate import rbf
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

from tonality.tonality_classes.Classificators.CNNClassificator import CNNClassificator
from tonality.tonality_classes.Classificators.ClassificatorFactory.ClassificatorFactory import ClassificatorFactory
from tonality.tonality_classes.Classificators.MlpClassificator import MlpClassificator
from tonality.tonality_classes.Classificators.NBClassificator import NBClassificator
from tonality.tonality_classes.Classificators.RFClassificator import RFClassificator
from tonality.tonality_classes.Classificators.RNNClassificator import RNNClassificator
from tonality.tonality_classes.Classificators.SVCClassificator import SVCClassificator
from tonality.tonality_classes.Vectors.VectorFactory.VectorFactory import VectorFactory
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

logging.root.handlers = []
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

import numpy as np
import gensim

from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
import nltk
# nltk.download('stopwords')
from nltk.corpus import stopwords

import re
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from keras import backend as K
from keras.layers import Dense, concatenate, Activation, Dropout, Bidirectional, LSTM, MaxPooling1D, Flatten
from keras.models import Model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from sklearn.metrics import classification_report

class BaseClassification:

    def __init__(self, wv_type):
        self.to_preprocess_path = ""
        self.train_data_preprocessed_path = ""
        self.my_tags = []
        self.cotags = {'ADJF': 'ADJ',  # pymorphy2: word2vec
                  'ADJS': 'ADJ',
                  'ADVB': 'ADV',
                  'COMP': 'ADV',
                  'GRND': 'VERB',
                  'INFN': 'VERB',
                  'NOUN': 'NOUN',
                  'PRED': 'ADV',
                  'PRTF': 'ADJ',
                  'PRTS': 'VERB',
                  'VERB': 'VERB',
                  'INTJ': 'INTJ',
                  'PART': 'PART'
                  }
        self.morph = pymorphy2.MorphAnalyzer()
        self.arrayOfEmptyColumns = []
        self.taged = True if wv_type == 'w2v_news' else False
        self.wv = self.set_wv(wv_type)
        self.wv_size = self.wv.get_size()
        self.SENTENCE_LENGTH = 70
        self.NUM = 15139
        self.type_classification = ""
        self.predict_one_classificator = ""


    def set_wv(self, wv_type):
        return VectorFactory.get_vector_model_based_type(wv_type)


    def prepare_data(self):
        pass

    def preprocess(self):
        pass

    def preprocess_one(self, text, flaf_taged=False):
        pass

    def set_predict_one_classificator(self, classificator_type, vector_type = None):
        if vector_type == None:
            vector_type = self.wv.get_type()
        self.predict_one_classificator = ClassificatorFactory.get_classificator_based_type(classificator_type, vector_type, self.type_classification)

    def unset_predict_one_classificator(self):
        self.predict_one_classificator = ""

    def isNaN(self, num):
        return num != num

    def preprocess_text(self, text):
        text = text.lower().replace("ё", "е")
        text = re.sub('((www\.[^\s]+)|(https?://[^\s]+))', '', text)
        text = re.sub('@[^\s]+', '', text)
        text = re.sub('[^a-zA-Zа-яА-Я]+', ' ', text)
        text = re.sub(' +', ' ', text)
        return text.strip()

    def w2v_tokenize_text_not_taged(self, wv, item_of_dataframe):
        tokens = []
        text = item_of_dataframe['text']
        for sent in nltk.sent_tokenize(text, language='russian'):
            for word in nltk.word_tokenize(sent, language='russian'):
                match = re.search(r"[а-яА-Яё-]+", word)
                if match == None:
                    continue
                word = word.lower()
                if word == 'не':
                    word = 'отрицание'
                if word in stopwords.words('russian'):  # deete stop words
                    continue
                # parse_result = morph.parse(word)[0]
                # word = parse_result.normal_form
                if word not in wv.vocab:
                    continue
                tokens.append(word)
        # if len(tokens) < 3:
        #     self.arrayOfEmptyColumns.append(item_of_dataframe['id'])
        return ' '.join(tokens)

    def plot_confusion_matrix(self, cm, title='Confusion matrix', name = 'confusion-matrix', cmap=plt.cm.Blues):
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(self.my_tags))
        target_names = self.my_tags
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)
        plt.tight_layout()
        plt.ylabel('Действительность')
        plt.xlabel('Предсказанное')
        plt.savefig('../result_images/'+ self.type_classification +'/'+name+'.png')
        plt.close()

    def evaluate_prediction(self, predictions, target, conf_matr_name = 'confusion_matrix', title='Матрица ошибок'):
        print('accuracy %s' % str(accuracy_score(target, predictions)))
        cm = confusion_matrix(target, predictions)
        print(' matrix of errors\n %s' % cm)
        print('{row=expected, col=predicted}')
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        self.plot_confusion_matrix(cm_normalized, title + ' ', conf_matr_name)
        print('f1score ', self.get_f1_score(cm))
        average = 'binary' if len(self.my_tags) == 2 else 'macro'
        print('f1score lib', f1_score(target, predictions, average=average))

    def predict(self, vectorizer, classifier, data):  # predskazat' po nashim dannim
        data_features = vectorizer.transform(data['title'])
        predictions = classifier.predict(data_features)
        target = data['tag']
        self.evaluate_prediction(predictions, target)

    def w2v_tokenize_text(self, wv, item_of_dataframe):
        tokens = []
        not_taged_tokens = []
        text = item_of_dataframe['text']
        global arrayOfEmptyColumns

        for sent in nltk.sent_tokenize(text, language='russian'):
            for word in nltk.word_tokenize(sent, language='russian'):
                match = re.search(r"[а-яА-Яё-]+", word)
                if match == None:
                    continue
                word = word.lower()
                if word == 'не':
                    word = 'отрицание'
                if word in stopwords.words('russian'):  # deete stop words
                    continue

                parse_result = self.morph.parse(word)[0]
                word = parse_result.normal_form
                parse_result = self.morph.parse(word)[0]

                if parse_result.tag.POS not in self.cotags.keys():
                    continue
                not_taged_word = word
                word = word + '_' + self.cotags[parse_result.tag.POS]  # dobavili chast' rechi
                if word not in wv.vocab:
                    continue
                tokens.append(word)
                not_taged_tokens.append(not_taged_word)
        if len(tokens) < 3:
            self.arrayOfEmptyColumns.append(item_of_dataframe['id'])
        return tokens, not_taged_tokens

    def word_averaging(self, wv, words):
        if isinstance(words, str):
            words = self.get_precomputed_list(words)
        all_words, mean = set(), []
        for word in words:
            if isinstance(word, np.ndarray):
                mean.append(word)
            elif word in wv.vocab:
                mean.append(wv.syn0norm[wv.vocab[word].index])
                all_words.add(wv.vocab[word].index)

        if not mean:
            logging.warning("cannot compute similarity with no input %s", words)
            # FIXME: remove these examples in pre-processing
            return np.zeros(self.wv_size)
        mean = gensim.matutils.unitvec(np.array(mean).mean(axis=0)).astype(np.float32)
        return mean  # poluchile sredniy vector

    def word_averaging_list(self, wv, text_list):
        return np.vstack([self.word_averaging(wv, review) for review in text_list])

    def cousine_distance(self, item1, item2):
        return np.dot(item1, item2) / (np.linalg.norm(item1) * np.linalg.norm(item2))

    def get_precomputed_list(self,text):
        line = re.sub(r"[\[\]',]+", "", text)
        return line.split(' ')

    def get_precision(self, cm):
        sum = 0
        for i in range(0, len(cm[0, :])):
            pres = cm[i, i] / cm[i, :].sum()
            sum += pres
            print('precision of class ' + str(i) + ' = ' + str(pres))
        return sum / len(cm[0, :])

    def get_recall(self, cm):
        sum = 0
        for i in range(0, len(cm[0, :])):
            pres = cm[i, i] / cm[:, i].sum()
            sum += pres
            print('recall of class ' + str(i) + ' = ' + str(pres))
        return sum / len(cm[0, :])

    def get_f1_score(self, cm):
        recall = self.get_recall(cm)
        precision = self.get_precision(cm)
        return (2 * precision * recall) / (precision + recall)


    def plot_metrix(self, ax, x1, x2, title):
        ax.plot(range(1, len(x1) + 1), x1, label='train')
        ax.plot(range(1, len(x2) + 1), x2, label='val')
        ax.set_ylabel(title)
        ax.set_xlabel('Epoch')
        ax.legend()
        ax.margins(0)

    def plot_history(self, history, history_name):
        fig, axes = plt.subplots(ncols=2, nrows=2, figsize=(16, 9))
        ax1, ax2, ax3, ax4 = axes.ravel()
        self.plot_metrix(ax1, history.history['precision'], history.history['val_precision'], 'Precision')
        self.plot_metrix(ax2, history.history['recall'], history.history['val_recall'], 'Recall')
        self.plot_metrix(ax3, history.history['f1'], history.history['val_f1'], "$F_1$")
        self.plot_metrix(ax4, history.history['loss'], history.history['val_loss'], 'Loss')
        plt.show()
        plt.savefig(history_name)
        plt.close()

    def build_embeding_matrix(self, tokenizer):
        embedding_matrix = np.zeros((self.NUM, self.wv_size))
        # Добавляем NUM=100000 наиболее часто встречающихся слов из обучающей выборки в embedding слой
        wv_model = self.wv.get_wv()
        # os.sys.exit()
        for word, i in tokenizer.word_index.items():
            print(word)
            if i > self.NUM:
                break
            if self.taged:
                parse_result = self.morph.parse(word)[0]
                word = parse_result.normal_form
                parse_result = self.morph.parse(word)[0]
                if parse_result.tag.POS not in self.cotags.keys():
                    continue
                word = word + '_' + self.cotags[parse_result.tag.POS]  # dobavili chast' rechi

            if word not in wv_model.vocab:
                print(word)
                continue
            embedding_matrix[i - 1] = wv_model.wv[word]

        print(embedding_matrix)
        return embedding_matrix

    def get_sequences(self, tokenizer, x):
        sequences = tokenizer.texts_to_sequences(x)
        return pad_sequences(sequences, maxlen=self.SENTENCE_LENGTH)

    def train(self):
        print(self.train_data_preprocessed_path)
        df3 = pd.read_csv(self.train_data_preprocessed_path)
        df3.dropna()
        train_data, test_data = train_test_split(df3, test_size=0.2, random_state=42)
        # print(test_data)
        test_data.tag.value_counts().plot(kind="bar", rot=0)
        plt.savefig('../result_images/' + self.type_classification + '/test_data_counts.png')
        plt.close()
        train_data.tag.value_counts().plot(kind="bar", rot=0)
        plt.savefig('../result_images/' + self.type_classification + '/train_data_counts.png')
        plt.close()
        wv = self.wv.get_wv()
        # X_train_word_average = self.word_averaging_list(wv, train_data['text'])
        #
        # X_test_word_average = self.word_averaging_list(wv, test_data['text'])

        print('обучение началось')


        # TF IDF
        # tf_vect = TfidfVectorizer(min_df=2, tokenizer=nltk.word_tokenize, preprocessor=None)
        # train_data_features = tf_vect.fit_transform(
        #     train_data['title'] if self.wv.get_type() == 'w2v_news' else train_data['text'])
        #
        # pickle.dump(tf_vect, open("../trained_tokenizers/IDF_"+ self.wv.get_type() +"_feature_" + self.type_classification +".pkl", "wb"))
        #
        # test_data_features = tf_vect.transform(test_data['title'] if self.wv.get_type() == 'w2v_news' else test_data['text'])

        #
        # mlp = MlpClassificator("MLP", self.wv.get_type(), self.type_classification)
        # mlp.load_classificator()
        # mlp.fit(X_train_word_average, train_data['tag'])
        #
        # random_forest = RFClassificator("RF", self.wv.get_type(), self.type_classification)
        # random_forest.load_classificator()
        # random_forest.fit(X_train_word_average, train_data['tag'])

        # nb = NBClassificator("NB", self.wv.get_type(), self.type_classification)
        # nb.load_classificator()
        # nb.fit(X_train_word_average, train_data['tag'])
        #
        #
        # svc = SVCClassificator("SVC", self.wv.get_type(), self.type_classification)
        # svc.load_classificator()
        # svc.fit(X_train_word_average, train_data['tag'])
        # TF IDF
        #
        # mlpIDF = MlpClassificator("MLP", 'IDF_' + self.wv.get_type(), self.type_classification)
        # mlpIDF.load_classificator()
        # mlpIDF.fit(train_data_features, train_data['tag'])
        # svcIDF = SVCClassificator("SVC", 'IDF_' + self.wv.get_type()  , self.type_classification)
        # svcIDF.load_classificator()
        # svcIDF.fit(train_data_features, train_data['tag'])
        #
        # mlp.save_model()
        # random_forest.save_model()
        # svc.save_model()
        # nb.save_model()
        # mlpIDF.save_model()
        # svcIDF.save_model()
        # self.test_predict(X_test_word_average, test_data, test_data_features)

        self.train_nn(train_data, test_data, 'CNN')

    def train_nn(self, train_data, test_data, type = 'CNN'):
        self.my_tags = [0, 1, 2] if self.type_classification == 'news' else [0,1]
        if self.type_classification == 'news':
            train_data.loc[(train_data.tag == 0, 'tag')] = 2
            test_data.loc[(test_data.tag == 0, 'tag')] = 2
        train_data.loc[(train_data.tag == -1, 'tag')] = 0
        test_data.loc[(test_data.tag == -1, 'tag')] = 0
        x_train = train_data['title'] if self.taged else train_data['text']
        x_test = test_data['title'] if self.taged else test_data['text']
        y_train, y_test = train_data['tag'], test_data['tag']

        tokenizer = Tokenizer(num_words=self.NUM)
        # print(x_train)
        # os.sys.exit()
        x_train.dropna()

        tokenizer.fit_on_texts(x_train)

        with open('../trained_tokenizers/nn_tokenizer_' + self.wv.get_type() + '_' + self.type_classification +'.pickle', 'wb') as handle:
            pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)
        self.tokenizer = tokenizer
        x_train_seq = self.get_sequences(tokenizer, x_train)
        #
        # embedding_matrix = self.build_embeding_matrix(tokenizer)
        # if type == 'CNN':
        #     model = CNNClassificator('CNN', self.wv.get_type(), self.type_classification)
        # else:
        #     model = RNNClassificator('RNN', self.wv.get_type(), self.type_classification)
        # model.load_classificator_nn(self.SENTENCE_LENGTH, self.NUM, self.wv_size, embedding_matrix)
        # print(y_train)
        # os.sys.exit()
        # # #
        # history = model.fit(x_train_seq, y_train)

        #
        # plt.style.use('ggplot')
        # self.plot_history(history, "../result_images/"+ self.type_classification +"/first_history_" + self.wv.get_type() +"_"+ type + ".png")
        # model.save_model()
        #
        self.over_train(x_train_seq, y_train, '07-0.71', 'CNN')

        self.test_predict_nn(x_test, y_test, 'CNN')


    def over_train(self, x_train_seq, y_train, weights_params, type = 'CNN'):

        if type == 'CNN':
            model = CNNClassificator('CNN', self.wv.get_type(), self.type_classification)
        else:
            model = RNNClassificator('RNN', self.wv.get_type(), self.type_classification)

        history_trainable = model.over_train_model(x_train_seq, y_train, weights_params)
        self.plot_history(history_trainable, "../result_images/"+ self.type_classification +"/first_history_" + self.wv.get_type() +"_CNN.png")
        model.save_model()
        return model


    def test_predict(self, X_test_word_average, test_data, test_data_features):
        mlp = MlpClassificator("MLP", self.wv.get_type(), self.type_classification)
        mlp.get_saved_model()
        random_forest = RFClassificator("RF", self.wv.get_type(), self.type_classification)
        random_forest.get_saved_model()
        # nb = NBClassificator("NB", self.wv.get_type(), self.type_classification)
        # nb.get_saved_model()
        svc = SVCClassificator("SVC", self.wv.get_type(), self.type_classification)
        svc.get_saved_model()
        mlpIDF = MlpClassificator("MLP", 'IDF_' + self.wv.get_type(), self.type_classification)
        mlpIDF.get_saved_model()
        svcIDF = SVCClassificator("SVC", 'IDF_' + self.wv.get_type(), self.type_classification)
        svcIDF.get_saved_model()


        predicted0 = mlp.predict(X_test_word_average)
        predicted1 = random_forest.predict(X_test_word_average)
        predicted2 = svc.predict(X_test_word_average)
        # predicted5 = nb.predict(X_test_word_average)
        predicted3 = mlpIDF.predict(test_data_features)
        predicted4 = svcIDF.predict(test_data_features)


        print('MLP predictions')
        self.evaluate_prediction(predicted0, test_data.tag,
                                 'confusion_matr_'+ self.wv.get_type() + '_' + mlp.get_type())
        print('RF predictions')
        self.evaluate_prediction(predicted1, test_data.tag,
                                 'confusion_matr_' + self.wv.get_type() + '_' + random_forest.get_type())
        # print('NB predictions')
        # self.evaluate_prediction(predicted5, test_data.tag,
        #                          'confusion_matr_' + self.wv.get_type() + '_' + random_forest.get_type())
        print('SVC predictions')
        self.evaluate_prediction(predicted2, test_data.tag,
                                 'confusion_matr_' + self.wv.get_type() + '_' + svc.get_type())
        print('MLP IDF predictions')
        self.evaluate_prediction(predicted3, test_data.tag, 'confusion_matr_' + 'IDF' + '_' + mlpIDF.get_type())
        print('SVC IDF predictions')
        self.evaluate_prediction(predicted4, test_data.tag, 'confusion_matr_' + 'IDF' + '_' + svcIDF.get_type())

        ind_of_film = 7
        predicted0 = mlp.predict_proba(np.asmatrix(X_test_word_average[ind_of_film]))
        predicted1 = random_forest.predict_proba(np.asmatrix(X_test_word_average[ind_of_film]))
        test_array_plot = np.asarray(test_data['text'])
        test_tag_array = np.asarray(test_data['tag'])
        print("plot of the film : ")
        print(test_data.iloc()[ind_of_film]['text'])
        print("real genre of the film = " + str(test_tag_array[ind_of_film]))
        print("classifier answer RF = " + str(predicted1))
        print("classifier answer of NLP = " + str(predicted0))
        print("classifier answer RF = " + str(random_forest.predict(np.asmatrix(X_test_word_average[ind_of_film]))))
        print("classifier answer of NLP = " + str(mlp.predict(np.asmatrix(X_test_word_average[ind_of_film]))))




    def test_predict_nn(self, x_test, y_test, type ='CNN'):

        with open('../trained_tokenizers/nn_tokenizer_'+ self.wv.get_type() + '_' + self.type_classification + '.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)
        x_test_seq = self.get_sequences(tokenizer, x_test)
        if type == 'CNN':
            model = CNNClassificator('CNN', self.wv.get_type(), self.type_classification)
        else:
            model = RNNClassificator('RNN', self.wv.get_type(), self.type_classification)
        model.get_saved_model()
        predicted = np.round(model.predict(x_test_seq))
        print(classification_report(y_test, predicted, digits=2))


        print(x_test)
        # ind_of_film = 36832
        # print(x_test[ind_of_film])
        # print(y_test[ind_of_film])
        # print(x_test)
        # pandas = pd.Series([x_test[ind_of_film]]).astype(str)
        # print(pandas)
        print('pandas2')
        pandas = pd.Series(['немного мыть опыт поиск идеальный подгузник трусик трусик кораблик трусик девочка подгузник кораблик подгузник'])
        print(pandas)
        x_one_test_seq = self.get_sequences(tokenizer, pandas)
        print(x_one_test_seq)
        predict_one = model.predict(np.asarray([x_one_test_seq[0]]))
        print('predicted = ' + str(np.round(predict_one)))
        predicted = np.squeeze(np.asarray(predicted, dtype=int))
        self.evaluate_prediction(predicted, y_test, 'confusion_matr_' + self.wv.get_type() + '_' + model.get_type())




    def get_test_feature_based_type(self, text, vector_type, nn = False):
        if nn:
            # print(text)
            with open('../trained_tokenizers/nn_tokenizer_'+ self.wv.get_type()+ '_' + self.type_classification +'.pickle', 'rb') as handle:
                tokenizer = pickle.load(handle)
            pandas = pd.Series([' '.join(text)]).astype(str)
            x_one_test_seq = self.get_sequences(tokenizer, pandas)
            # print(x_one_test_seq)
            test_data_features = np.asarray([x_one_test_seq[0]])

        elif 'IDF' in vector_type:
            tf_vect = pickle.load(open("../trained_tokenizers/IDF_"+ self.wv.get_type() +"_feature_" + self.type_classification +".pkl", 'rb'))
            test_data = pd.DataFrame({'text' : ''}, index=[0])
            test_data1 = test_data.iloc[0]
            test_data1['text'] = ' '.join(text)
            test_data = pd.concat([test_data1])
            test_data_features = tf_vect.transform([test_data['text']])
        else:
            test_data_features = [self.word_averaging(self.wv.get_wv(), text)]

        return test_data_features


    def predict_one(self, text, classificator_type, vector_type=None):

        if vector_type == None:
            vector_type = self.wv.get_type()
            self.taged = True if vector_type == 'w2v_news' else False
        elif vector_type == 'IDF':
            vector_type = vector_type + '_' + self.wv.get_type()
            self.taged = False
        preprocessed_text = self.preprocess_one(text, self.taged)
        if self.predict_one_classificator == "":
            classificator = ClassificatorFactory.get_classificator_based_type(classificator_type, vector_type,
                                                                          self.type_classification)
        else:
            classificator = self.predict_one_classificator

        nn = True if classificator_type == 'CNN' else False
        # print(preprocessed_text)
        # print(vector_type)
        vector_based_type = self.get_test_feature_based_type(preprocessed_text, vector_type, nn)
        # print(vector_based_type)
        # print(classificator.predict_proba(vector_based_type))
        if nn:
            return classificator.predict(vector_based_type)
        else:
            return classificator.predict(vector_based_type)

    def get_cascade_decision(self, text):
        decision1 = self.predict_one(text, 'SVC')
        decision2 = self.predict_one(text, "MLP")
        decision3 = self.predict_one(text, 'RF')
        decision4 = self.predict_one(text, 'MLP', 'IDF')
        decision5 = self.predict_one(text, 'SVC', 'IDF')
        sum = decision1[0] + decision2[0] + decision3[0] + decision4[0] + decision5[0]
        return (sum / 5 + 1) / 2
