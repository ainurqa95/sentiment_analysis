import os

import pandas as pd
import matplotlib.pyplot as plt
from tonality.tonality_classes.ClassificationTypes.BaseClassification import BaseClassification


class NewsClassification(BaseClassification):

    def prepare_data(self):
        self.type_classification = "news"
        self.to_preprocess_path = '../data_news/train.csv'
        print(self.taged)
        taged_name = "taged" if self.taged else "not_taged"
        self.train_data_preprocessed_path = '../parse_content/news_precomputed' + '_' + self.wv.get_type() + '_' + taged_name + '.csv'
        self.my_tags = [-1, 0, 1]
        self.SENTENCE_LENGTH = 100
        self.NUM = 15139

    def change_tag(self, tag):
        if tag == 'negative':
            return -1
        elif tag == 'positive':
            return 1
        else:
            return 0

    def preprocess(self):
        df3 = pd.read_csv(self.to_preprocess_path)
        type = self.wv.get_type()
        wv = self.wv.get_wv()
        df = pd.DataFrame(columns=['id', 'text', 'title', 'tag'])
        if (type == 'w2v_news'):
            for index, row in df3.iterrows():
                taged_tokens, not_taged = self.w2v_tokenize_text(wv, row)
                df = df.append({'id': row['id'], 'text': taged_tokens, 'title': " ".join(not_taged),
                                'tag': self.change_tag(row['sentiment'])},
                               ignore_index=True)

        else:
            for index, row in df3.iterrows():
                taged_tokens = self.preprocess_text(row['text'])
                not_taged = taged_tokens
                df = df.append({'id': row['id'], 'text': taged_tokens, 'title': " ".join(not_taged), 'tag': row['tag']},
                           ignore_index=True)

        df['tag'].value_counts().plot(kind="bar", rot=0)
        plt.savefig('../result_images/' + self.type_classification + '/data_counts.png')
        plt.close()

        for item in self.arrayOfEmptyColumns:
            df = df[df.id != item]
        df.to_csv(self.train_data_preprocessed_path)


    def preprocess_one(self, text, flag_taged=False):
        wv = self.wv.get_wv()
        type = self.wv.get_type()
        row = {'text' : text}
        if (type == 'w2v_news'):
            taged_tokens, not_taged = self.w2v_tokenize_text(wv, row)
            if flag_taged:
                return taged_tokens
            else:
                return not_taged
        else:
            return self.preprocess_text(row['text'])
            # return self.w2v_tokenize_text_not_taged(wv, row)