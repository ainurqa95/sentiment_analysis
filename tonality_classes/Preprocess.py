from tonality.tonality_classes.ClassificationTypes.NewsClassification import NewsClassification
from tonality.tonality_classes.ClassificationTypes.ReviewsClassification import ReviewsClassification
from tonality.tonality_classes.ClassificationTypes.TweetClassification import TweetClassification
from tonality.tonality_classes.Vectors.WVNewsVec import WVNewsVec



# rc = ReviewsClassification("w2v_news")
# rc = ReviewsClassification("fast_text_areneum")
# rc.prepare_data()
# rc.preprocess()


tc = TweetClassification("w2v_tweet")
tc.prepare_data()
tc.preprocess()

nc = NewsClassification("w2v_news")
# nc = NewsClassification('fast_text_areneum')
nc.prepare_data()
nc.preprocess()










