from tonality.tonality_classes.Vectors.BaseVec import BaseVec

class GloveVec(BaseVec):
    def load_vec(self):
        from gensim.scripts.glove2word2vec import glove2word2vec
        from gensim.test.utils import datapath, get_tmpfile
        from gensim.models import KeyedVectors
        if not self.wvPath:
            self.wvPath = '/home/ainur/PycharmProjects/tonality/data_glove/multilingual_embeddings.ru'
        glove_file = datapath(self.wvPath)
        tmp_file = get_tmpfile("test_glove.txt")
        glove2word2vec(glove_file, tmp_file)
        self.wvType = "glove"
        self.wv = KeyedVectors.load_word2vec_format(tmp_file)
        self.wv.init_sims(replace=True)