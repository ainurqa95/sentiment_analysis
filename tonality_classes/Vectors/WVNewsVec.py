import gensim

from tonality.tonality_classes.Vectors.BaseVec import BaseVec


class WVNewsVec(BaseVec):
    def load_vec(self):
        if not self.wvPath:
            self.wvPath = '../data/ruscorpora_upos_skipgram_300_10_2017.bin'
            # self.wvPath = '../data/news_0_300_2.bin'
        self.wvType = "w2v_news"
        self.wv = gensim.models.KeyedVectors.load_word2vec_format(self.wvPath, binary=True)
        self.wv.init_sims(replace=True)
