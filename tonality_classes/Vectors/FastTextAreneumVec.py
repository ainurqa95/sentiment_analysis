from gensim.models import FastText

from tonality.tonality_classes.Vectors.BaseVec import BaseVec

class FastTextAreneumVec(BaseVec):
    def load_vec(self):
        self.wv_size = 300
        if not self.wvPath:
            self.wvPath = '../data_fast_text/areneum/araneum_none_fasttextskipgram_300_5_2018.model'
        self.wvType = "fast_text_areneum"
        self.wv = FastText.load(self.wvPath).wv
        self.wv.init_sims(replace=True)