import gensim
from gensim.models import Word2Vec

from tonality.tonality_classes.Vectors.BaseVec import BaseVec


class FastTextTweeeterVec(BaseVec):
    def load_vec(self):
        if not self.wvPath:
            self.wvPath = '../data_fast_text/tweet_fast_text.model")'
        self.wvType = "fast_text_tweeter"
        model = Word2Vec.load(self.wvPath).wv
        self.wv_size = model.vector_size
        self.wv = model.wv
