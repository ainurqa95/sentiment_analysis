from gensim.models import Word2Vec, FastText
from gensim.models.wrappers import Wordrank


class BaseVec:
    def __init__(self, wvPath):
        self.wvPath = wvPath
        self.wvType = "base"
        self.wv = ""
        self.wv_size = 300
    def load_vec(self):
        pass
    def get_wv(self):
        return self.wv
    def get_type(self):
        return self.wvType
    def get_size(self):
        return self.wv_size

    def load_model_based_type(type, model_name):
        if type == 'w2v':
            return Word2Vec.load("data_" + type + "/" + model_name + "_" + type + ".model")
        if type == 'fast_text':
            return FastText.load("data_" + type + "/" + model_name + "_" + type + ".model")
        if type == 'wordrank':
            return Wordrank.load("data_" + type + "/" + model_name + "_" + type + ".model")

