from tonality.tonality_classes.Vectors.FastTextAreneumVec import FastTextAreneumVec
from tonality.tonality_classes.Vectors.GloveVec import GloveVec
from tonality.tonality_classes.Vectors.W2VTweeterVec import W2VTweeterVec
from tonality.tonality_classes.Vectors.WVNewsVec import WVNewsVec


class VectorFactory:

    @staticmethod
    def get_vector_model_based_type(wv_type):
        if wv_type == 'w2v_news':
            wv = WVNewsVec("")
        elif wv_type == 'fast_text_areneum':
            wv = FastTextAreneumVec("")
        elif wv_type == 'glove':
            wv = GloveVec("")
        elif wv_type == 'w2v_tweet':
            wv = W2VTweeterVec("")
        elif wv_type == 'fast_text_tweeter':
            wv = W2VTweeterVec("")
        else:
            wv = WVNewsVec("")
        wv.load_vec()
        return wv
