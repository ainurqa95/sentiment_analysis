import gensim
from gensim.models import Word2Vec

from tonality.tonality_classes.Vectors.BaseVec import BaseVec


class W2VTweeterVec(BaseVec):
    def load_vec(self):
        if not self.wvPath:
            self.wvPath = '../data_w2v/tweet_w2v.model'
        self.wvType = "w2v_tweeter"
        model = Word2Vec.load(self.wvPath).wv
        self.wv_size = model.vector_size
        self.wv = model.wv
        self.wv.init_sims(replace=True)
