import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def isNaN(num):
    return num != num

filePath = "parse_content/new_data_texts.csv"
filePathMyData = "parse_content/result_positive2.csv"
myData = pd.read_csv(filePathMyData)
# myData = myData[myData.tag == '1']
for i, row in myData.iterrows(): # меняем разметку
    if row['rating_review'] <= 3:
        myData.at[i,'tag'] = '-1'
    if row['rating_review'] >= 4:
        myData.at[i,'tag'] = '1'
myData.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('data_counts1.png')
plt.close()

dfResult = pd.read_csv(filePath)
dfResult.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('data_counts2.png')
plt.close()
# removeIndexes = dfResult.index > 15000 and (dfResult.tag == '-1' and dfResult.tag == '0' )
# dfResult = dfResult[!removeIndexes]
# dfResult = myData
# dfResult = pd.concat([myData,dfResult], ignore_index=False, sort=True)
tagsSet = set()
rempve = ['0','nan', None,'21887', '23523', 'GalinaPozd', '23486', 'Виталия Салина', 'Арина Макковеева',  '22158', 'Минченкова Елизавета ', 'Анна Аникина', 'Yudenkova Dasha', 'Иван Мишалкин ','Анна Вейдер']
for item in rempve:
    dfResult = dfResult[dfResult.tag != item]
# print(df3)
l = 0

countPos = 0
countNeg = 0
for i, row in dfResult.iterrows(): # меняем разметку

    if row['tag'] == '-2':
        dfResult.at[i,'tag'] = '-1'
        # print(row)
    if row['tag'] == '2':
        dfResult.at[i, 'tag'] = '1'
    tagsSet.add(row['tag'])
    if row['tag'] in rempve:
        print(row)

    if isNaN(row['tag']) or isNaN(row['text']):
        dfResult = dfResult[dfResult.index != i]

    if row['tag'] == '-1':
        countNeg = countNeg + 1
        if countNeg > 4000 or countPos < countNeg - 2000:
            dfResult = dfResult[dfResult.index != i]
    if row['tag'] == '0':
        dfResult = dfResult[dfResult.index != i]
    if row['tag'] == '1':
        countPos = countPos + 1
        # if countPos > 10000  :
        #     dfResult = dfResult[dfResult.index != i]


print(countPos)
print('EEEEEND')


filtToSave = 'parse_content/result_new_text.csv'
dfResult.to_csv(filtToSave)

df3 = pd.read_csv(filtToSave)
df3.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('data_counts.png')
plt.close()

