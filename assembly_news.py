import os
from collections import defaultdict

import pandas as pd
import matplotlib.pyplot as plt

filePath = "data_news/train.json"
myData = pd.read_json(filePath)

print(myData.count())
dfPos = myData.loc[myData['sentiment'] == 'positive']
dfNet = myData.loc[myData['sentiment'] == 'neutral']
dfNeg = myData.loc[myData['sentiment'] == 'negative']
dfNet = dfNet.sample(frac=0.75)
dfResult = pd.concat([dfPos, dfNet, dfNeg])


filePath2 = "data_news/vesti_news.csv"
myData2 = pd.read_csv(filePath2)
dfResult2 = myData2.sample(frac=0.22)
d = defaultdict(lambda: 'Other')
dfResult2['sentiment'] = 'negative'
dfResult = pd.concat([dfResult, dfResult2], sort=True)
dfResult.loc[dfResult['sentiment'] == 'negative', 'tag'] = -1
dfResult.loc[dfResult['sentiment'] == 'positive', 'tag'] = 1
dfResult.loc[dfResult['sentiment'] == 'neutral', 'tag'] = 0
print(dfResult.count())
dfResult = dfResult.loc[dfResult['text'] == dfResult['text']] # Nan check

filePathSave = 'data_news/train.csv'
dfResult.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('data_counts_news.png')
plt.close()
dfResult.set_index('id').to_csv(filePathSave)