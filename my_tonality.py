#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  import modules and set up logging
import os
import sys
from pathlib import Path
import time
import xlrd
from gensim.models import word2vec, FastText
import logging
from scipy.interpolate import rbf


logging.root.handlers = []
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from smart_open import smart_open
from sklearn.externals import joblib
import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from numpy import random
import nltk
import pymorphy2
import regex
import gensim
from  itertools import islice
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import nltk
# nltk.download('stopwords')
from gensim.models import word2vec
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument
from sklearn.neighbors import KNeighborsClassifier
from nltk.corpus import stopwords
from sklearn import tree
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
import re
from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from keras import backend as K
from keras.layers import Dense, concatenate, Activation, Dropout, Bidirectional, LSTM, MaxPooling1D, Flatten
from keras.models import Model
from keras.layers.convolutional import Conv1D
from keras.layers.pooling import GlobalMaxPooling1D
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from sklearn.metrics import classification_report




logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
morph = pymorphy2.MorphAnalyzer()
punct = re.compile('^(.*?)([а-яА-ЯёЁ-]+)(.*?)$')

cotags = {'ADJF':'ADJ', # pymorphy2: word2vec
'ADJS' : 'ADJ',
'ADVB' : 'ADV',
'COMP' : 'ADV',
'GRND' : 'VERB',
'INFN' : 'VERB',
'NOUN' : 'NOUN',
'PRED' : 'ADV',
'PRTF' : 'ADJ',
'PRTS' : 'VERB',
'VERB' : 'VERB',
'INTJ' : 'INTJ',
'PART' : 'PART'
        }
def load_model_based_type(type, model_name):
    if type == 'w2v':
        return Word2Vec.load("data_" + type + "/" + model_name + "_"+ type +".model")
    if type == 'fast_text':
        return FastText.load("data_" + type + "/" + model_name + "_"+ type +".model")
    if type == 'wordrank':
        return Wordrank.load("data_" + type + "/" + model_name + "_"+ type +".model")
    if type == 'areneum_fast_text':
        return FastText.load("data_fast_text/areneum/araneum_none_fasttextskipgram_300_5_2018.model")

def tokenize_text (text): 
    tokens = [] 
    for sent in nltk.sent_tokenize(text):
        for word in nltk.word_tokenize(sent):
            if len(word)<2 :
                continue
            if word in stopwords.words('russian'):  # deete stop words
                continue
            tokens.append(word.lower())
    return tokens

def plot_confusion_matrix (cm, title='Confusion matrix',cmap=plt.cm.Blues):
    plt.imshow(cm,interpolation='nearest',cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks=np.arange(len(my_tags))
    target_names = my_tags
    plt.xticks(tick_marks,target_names,rotation=45)
    plt.yticks(tick_marks,target_names)
    plt.tight_layout()
    plt.ylabel('Действительность')
    plt.xlabel('Предсказанное')
    plt.savefig('result_images/confusion-matrix2.png')
    plt.close()

def evaluate_prediction (predictions,target,title = 'Матрица ошибок'):
    print ('accuracy %s' % str(accuracy_score(target,predictions)))
    # print(target)
    # print(predictions)
    # print(type(predictions))
    cm = confusion_matrix(target,predictions)
    print (' matrix of errors\n %s' % cm)
    print('{row=expected, col=predicted}')
    cm_normalized = cm.astype('float') /cm.sum(axis=1)[:,np.newaxis]
    plot_confusion_matrix(cm_normalized,title+ ' ')
    print('f1score ', get_f1_score(cm))
    # print('f1score lib', f1_score(target, predictions, average='macro'))
    print('f1score lib', f1_score(target, predictions, average='binary'))

def predict (vectorizer,classifier,data): # predskazat' po nashim dannim
    data_features = vectorizer.transform(data['title'])
    predictions = classifier.predict(data_features)
    target = data['tag']
    evaluate_prediction(predictions,target)



def w2v_tokenize_text(wv, item_of_dataframe):#schitaem skolko raz vstrechalas' slovo lubov' v etom scinarii nevazhno v kakom porydke idut slova
    tokens = [] # esli lubov' chashe chem planeta to skoree vsego romance
    text = item_of_dataframe['ttext']
    global arrayOfEmptyColumns
    for sent in nltk.sent_tokenize(text, language='russian'):
        for word in nltk.word_tokenize(sent, language='russian'):
            if len(word) < 3:
                continue
            match = re.search(r"[а-яА-Яё-]+", word)
            if match == None:
                continue
            word = word.lower()
            if word in stopwords.words('russian'):# deete stop words
                continue

            parse_result = morph.parse(word)[0]
            word = parse_result.normal_form
            parse_result = morph.parse(word)[0]

            if parse_result.tag.POS not in cotags.keys():

                continue
            word = word + '_' + cotags[parse_result.tag.POS]  # dobavili chast' rechi
            if word not in wv.vocab:
                continue
            tokens.append(word)
    if len(tokens) == 0:
        arrayOfEmptyColumns.append(item_of_dataframe['id'])
    return tokens



def word_averaging(wv, words):
    if isinstance(words,str):
        words = get_precomputed_list(words)
    all_words, mean = set(), []
    for word in words:
        if isinstance(word, np.ndarray):
            mean.append(word)
        elif word in wv.vocab:
            mean.append(wv.syn0norm[wv.vocab[word].index])
            all_words.add(wv.vocab[word].index)

    if not mean:
        logging.warning("cannot compute similarity with no input %s", words)
        # FIXME: remove these examples in pre-processing
        return np.zeros(300)
    mean = gensim.matutils.unitvec(np.array(mean).mean(axis=0)).astype(np.float32)

    return mean #poluchile sredniy vector


def word_averaging_list (wv, text_list):
    return np.vstack([word_averaging(wv,review) for review in text_list])

def cousine_distance (item1, item2):
    return  np.dot(item1, item2) / ( np.linalg.norm(item1) * np.linalg.norm(item2))

def get_precomputed_list(text):
    line = re.sub(r"[\[\]',]+", "", text)
    return line.split(' ')


def get_precision(cm):
    sum = 0
    for i in range(0,len(cm[0,:])):
        pres = cm[i,i] / cm[i,:].sum()
        sum += pres
        print('precision of class ' + str(i) + ' = ' + str(pres))
    return sum / len(cm[0,:])

def get_recall(cm):
    sum = 0
    for i in range(0,len(cm[0,:])):
        pres =  cm[i,i] / cm[:,i].sum()
        sum += pres
        print('recall of class ' + str(i) + ' = ' + str(pres))
    return sum/len(cm[0,:])

def get_f1_score(cm):
    recall = get_recall(cm)
    precision = get_precision(cm)
    return (2 * precision * recall) / (precision + recall)

def precision(y_true, y_pred):
    """Precision metric.

    Only computes a batch-wise average of precision.

    Computes the precision, a metric for multi-label classification of
    how many selected items are relevant.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def recall(y_true, y_pred):
    """Recall metric.

    Only computes a batch-wise average of recall.

    Computes the recall, a metric for multi-label classification of
    how many relevant items are selected.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


def txt_read_kadr():
    path = '/home/ainur/PycharmProjects/word2Vec/test/test1.txt'
    my_file = Path(path)
    if not my_file.exists():
        return

    f = open(path, 'r', encoding='utf-8')

    document = str(" ")

    for line in f:
        line = line.strip()
        words = line.split(' ')
        for word in words:
            document+= word+' '

        result = re.findall(r'[а-яА-Яё' ']+', document)
        document1 = ""
        for res in result: # оставяем только слова и пробелы
            document1 += res + ' '
    return document1





arrayOfEmptyColumns = []

# test = np.asmatrix([[1.0], [0.0], [3.0]])
# print(np.squeeze(np.asarray(test, dtype=int)))
# os.sys.exit()
df3 = pd.read_csv('parse_content/reviews_precomputed_w2v_news_taged.csv')
# df3 = pd.read_csv('parse_content/new_texts_precomputed.csv')
df3.dropna()
print(df3['text'].apply(lambda x: len(x.split(' '))).sum())  # chislo slov v file
print(df3['tag'])
print(df3['text'])
# print(len(df3))

# my_tags = ['комедия', 'драма','боевик']
my_tags = [0,1]
df3.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('result_images/data_counts.png')
plt.close()

train_data,test_data = train_test_split(df3, test_size=0.2,random_state=42)
test_data.tag.value_counts().plot(kind="bar",rot=0)
plt.savefig('result_images/test_data_counts.png')
plt.close()

train_data.tag.value_counts().plot(kind="bar",rot=0)
plt.savefig('result_images/train_data_counts.png')
plt.close()


# test_doc = txt_read_kadr()
# print(test_doc)
t1 = time.time()

# wv = gensim.models.KeyedVectors.load_word2vec_format('data/tayga_1_2.vec')
# wv = gensim.models.KeyedVectors.load_word2vec_format('data/news_upos_cbow_600_2_2018.vec')
# wv = gensim.models.KeyedVectors.load_word2vec_format('data/ruwikiruscorpora_upos_skipgram_300_2_2018.vec.gz')
# wv = gensim.models.KeyedVectors.load_word2vec_format('data/ruwikiruscorpora_upos_skipgram_300_2_2018.vec.gz')
# wv = gensim.models.KeyedVectors.load_word2vec_format('data/news_0_300_2.bin', binary=True)

# GLOVE
# from gensim.scripts.glove2word2vec import glove2word2vec
# from gensim.test.utils import datapath, get_tmpfile
# from gensim.models import KeyedVectors
# glove_file = datapath('/home/ainur/PycharmProjects/tonality/data_glove/multilingual_embeddings.ru')
# tmp_file = get_tmpfile("test_word2vec.txt")
# glove2word2vec(glove_file, tmp_file)
# wv = KeyedVectors.load_word2vec_format(tmp_file)

glove = False
# OUR W2
# w2v_model = load_model_based_type('w2v', 'our_small')
# wv = w2v_model.wv

#OUR FAST text
# ft_model = load_model_based_type('fast_text','our_small')
# wv = ft_model.wv

# FAST TEXT areneum
# ar_ft_model = load_model_based_type('areneum_fast_text','')
# wv = ar_ft_model.wv
wv = gensim.models.KeyedVectors.load_word2vec_format('data/ruscorpora_upos_skipgram_300_10_2017.bin', binary=True)
wv.init_sims(replace=True)
# print(wv.most_similar(positive='снабженец', topn=5))
# print(wv.most_similar(positive='хороший', topn=5))
# os.sys.exit()

print(time.time() - t1)
# #
# # # print list(islice(wv.vocab,13000,13020))
# print(test_tokenized)
# print(test_data['ttext'])
# test_tokenized = test_data.apply(lambda r: w3v_tokenize_text(wv,r), axis=1)
# train_tokenized = train_data.apply(lambda r: w2v_tokenize_text(wv, r), axis=1)
if glove:
    test_tokenized = test_data['title']
    train_tokenized = train_data['title']
else:
    test_tokenized = test_data['text']
    train_tokenized = train_data['text']
print(test_tokenized)
# print(arrayOfEmptyColumns)
for item in arrayOfEmptyColumns:
    df3 = df3[df3.id != item]
# df3.set_index('id').to_csv('data/positive_negative_precomputed.csv')


X_train_word_average = word_averaging_list(wv,train_tokenized)

X_test_word_average = word_averaging_list(wv,test_tokenized)


# tokinezed_test_doc = w2v_tokenize_text(test_doc)
# test_doc_average = word_averaging(wv,tokinezed_test_doc)
# print(test_doc_average)


# print(wv.most_similar(positive=[wv['снабженец_NOUN']], topn=5))
# print(wv['снабженец_NOUN'])
# print(X_test_word_average[0])


# print(wv.similarity(X_test_word_average[1], wv['снабженец_NOUN']))
# print cosine_similarity = numpy.dot(model['spain'], model['france'])/(numpy.linalg.norm(model['spain'])* numpy.linalg.norm(model['france']))
print('обучение началось')
# logreg = linear_model.LogisticRegression(n_jobs=1, C=1e5)
# logreg = logreg.fit(X_train_word_average,train_data['tag']) #
mlp = MLPClassifier(solver='lbfgs', alpha=1e-8, hidden_layer_sizes=(6, 6), random_state=2)
mlp = mlp.fit(X_train_word_average,train_data['tag'])

random_forest = RandomForestClassifier(n_estimators=25)
random_forest = random_forest.fit(X_train_word_average,train_data['tag'])

svc = LinearSVC()
svc = svc.fit(X_train_word_average,train_data['tag'])
# TF IDF
tf_vect = TfidfVectorizer(
    min_df=2, tokenizer=nltk.word_tokenize,
    preprocessor=None)
train_data_features = tf_vect.fit_transform(train_data['title'])

mlpIDF = MLPClassifier(solver='lbfgs', alpha=1e-8, hidden_layer_sizes=(6, 6), random_state=2)
mlpIDF = mlpIDF.fit(train_data_features,train_data['tag'])
svcIDF = LinearSVC()
svcIDF = svcIDF.fit(train_data_features,train_data['tag'])
test_data_features = tf_vect.transform(test_data['title'])
#END TF IDF


predicted2 = mlp.predict(X_test_word_average)
predicted3 = random_forest.predict(X_test_word_average)
predicted0 = svc.predict(X_test_word_average)
predicted4 = mlpIDF.predict(test_data_features)
predicted5 = svcIDF.predict(test_data_features)

def getIdsOfNotEqual(predicted, tags):
    failedIds = []
    j = 0
    for i, row in tags.iteritems():  # меняем разметку
        if row != predicted[j]:
            failedIds.append(i)
        j = j + 1
    return failedIds


# failedIdsMlp = getIdsOfNotEqual(predicted2, test_data.tag)
# failedIdsRF = getIdsOfNotEqual(predicted3, test_data.tag)
# failedIdsSVC = getIdsOfNotEqual(predicted0, test_data.tag)
# failedIdsIDFMlp = getIdsOfNotEqual(predicted4,test_data.tag)
# intesrsectFailedIds = list(set(failedIdsMlp) & set(failedIdsRF) & set(failedIdsSVC) & set(failedIdsIDFMlp))
# print(intesrsectFailedIds)

# for i, row in test_data.iterrows():  # меняем разметку
#     if i in intesrsectFailedIds:
#         print(i)
#         print(row['id'])
#         print(row['title'])
#         print(row['tag'])


# evaluate_prediction(predicted,test_data.tag)
print('MLP predictions')
evaluate_prediction(predicted2,test_data.tag)
print('RF predictions')
evaluate_prediction(predicted3,test_data.tag)
print('SVC predictions')
evaluate_prediction(predicted0,test_data.tag)

print('MLP IDF predictions')
evaluate_prediction(predicted4,test_data.tag)

print('SVC IDF predictions')
evaluate_prediction(predicted5,test_data.tag)


# os.sys.exit()

ind_of_film = 7
predicted = mlp.predict_proba(np.asmatrix(X_test_word_average[ind_of_film]))
predicted3 = random_forest.predict_proba(np.asmatrix(X_test_word_average[ind_of_film]))
test_array_plot = np.asarray(test_data['text'])
test_tag_array = np.asarray(test_data['tag'])
print ("plot of the film : ")
print (test_data.iloc()[ind_of_film]['text'])
print ("real genre of the film = " + str(test_tag_array[ind_of_film]))
print ("classifier answer RF = " + str(predicted3))
print ("classifier answer of NLP = " + str(predicted))
print ("classifier answer RF = " + str(random_forest.predict(np.asmatrix(X_test_word_average[ind_of_film]))))
print ("classifier answer of NLP = " + str(mlp.predict(np.asmatrix(X_test_word_average[ind_of_film]))))

joblib.dump(mlp, 'trained_models/classificatorMLP.pkl') # после обучения запихиваем обученный в файл
joblib.dump(random_forest, 'trained_models/classificatorRF.pkl') # после обучения запихиваем обученный в файл
os.sys.exit()

train_data.loc[(train_data.tag == -1, 'tag')] = 0
test_data.loc[(test_data.tag == -1, 'tag')] = 0
x_train, x_test = train_data['title'], test_data['title']
y_train, y_test = train_data['tag'], test_data['tag']


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
# SENTENCE_LENGTH = 70
SENTENCE_LENGTH = 26
# NUM = 15139
NUM = 100000

def get_sequences(tokenizer, x):
    sequences = tokenizer.texts_to_sequences(x)
    return pad_sequences(sequences, maxlen=SENTENCE_LENGTH)

tokenizer = Tokenizer(num_words=NUM)
tokenizer.fit_on_texts(x_train)


x_train_seq = get_sequences(tokenizer, x_train)
# print(x_train_seq)
# os.sys.exit
x_test_seq = get_sequences(tokenizer, x_test)

print(x_test_seq[0])
# os.sys.exit()
from gensim.models import Word2Vec
# Загружаем обученную модель
w2v_model = wv
DIM = w2v_model.vector_size
# Инициализируем матрицу embedding слоя нулями
embedding_matrix = np.zeros((NUM, DIM))
# Добавляем NUM=100000 наиболее часто встречающихся слов из обучающей выборки в embedding слой
for word, i in tokenizer.word_index.items():
    print(i)
    # print(word)
    if i > NUM:
        break
    parse_result = morph.parse(word)[0]
    word = parse_result.normal_form
    parse_result = morph.parse(word)[0]
    if parse_result.tag.POS not in cotags.keys():
        continue
    if glove:
        word = word
    else:
        word = word + '_' + cotags[parse_result.tag.POS]  # dobavili chast' rechi
    if word not in wv.vocab:
        continue
    embedding_matrix[i-1] = w2v_model.wv[word]

print(embedding_matrix)

# os.sys.exit()

from keras.layers import Input
from keras.layers.embeddings import Embedding
#
tweet_input = Input(shape=(SENTENCE_LENGTH,), dtype='int32')
tweet_encoder = Embedding(NUM, DIM, input_length=SENTENCE_LENGTH,
                          weights=[embedding_matrix], trainable=False)(tweet_input)

# RNN

# l_lstm = Bidirectional(LSTM(70, dropout=0.2, recurrent_dropout=0.2))(tweet_encoder)
# preds = Dense(1, activation='sigmoid')(l_lstm)
# model = Model(tweet_input, preds)
# model.compile(loss='binary_crossentropy',optimizer='rmsprop',   metrics=[precision, recall, f1])

# CNN
branches = []
x = Dropout(0.2)(tweet_encoder)

for size, filters_count in [(2, 10), (3, 10), (4, 10), (5, 10)]:
    for i in range(filters_count):
        branch = Conv1D(filters=1, kernel_size=size, padding='valid', activation='relu')(x)
        branch = GlobalMaxPooling1D()(branch)
        branches.append(branch)

x = concatenate(branches, axis=1)
x = Dropout(0.2)(x)
x = Dense(30, activation='relu')(x)
x = Dense(1)(x)
output = Activation('sigmoid')(x)
model = Model(inputs=[tweet_input], outputs=[output])
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=[precision, recall, f1])
model.summary()

# CNN2
# print(embedding_matrix.shape)
print(tweet_encoder.shape)
# os.sys.exit()
l_cov1= Conv1D(128, 5, activation='relu')(tweet_encoder)
l_pool1 = MaxPooling1D(4)(l_cov1)
l_cov2 = Conv1D(128, 5, activation='relu')(l_pool1)
l_pool2 = MaxPooling1D(2)(l_cov2)
l_cov3 = Conv1D(128, 5, activation='relu')(l_pool2)
l_pool3 = MaxPooling1D(2)(l_cov3)  # global max pooling
l_flat = Flatten()(l_pool3)
l_dense = Dense(128, activation='relu')(l_flat)
preds = Dense(1, activation='sigmoid')(l_dense)
model = Model(tweet_input, preds)
model.compile(loss='binary_crossentropy',optimizer='adam',  metrics=[precision, recall, f1])

checkpoint = ModelCheckpoint("models/cnn/cnn-frozen-embeddings-{epoch:02d}-{val_f1:.2f}.hdf5",
                             monitor='val_f1', save_best_only=True, mode='max', period=1)
# checkpointRNN = ModelCheckpoint("models/rnn/rnn-frozen-embeddings-{epoch:02d}-{val_f1:.2f}.hdf5",
#                              monitor='val_f1', save_best_only=True, mode='max', period=1)
checkpointCNN2 = ModelCheckpoint("models/cnn2/cnn-frozen-embeddings-{epoch:02d}-{val_f1:.2f}.hdf5",
                             monitor='val_f1', save_best_only=True, mode='max', period=1)
history = model.fit(x_train_seq, y_train, batch_size=32, epochs=13, validation_split=0.25, callbacks = [checkpoint])
# historyRNN = model.fit(x_train_seq, y_train, batch_size=32, epochs=9, validation_split=0.2, callbacks = [checkpointRNN])
# historyCNN2 = model.fit(x_train_seq, y_train, batch_size=32, epochs=10, validation_split=0.25, callbacks = [checkpointCNN2])


import numpy as np
import matplotlib.pyplot as plt

plt.style.use('ggplot')


def plot_metrix(ax, x1, x2, title):
    ax.plot(range(1, len(x1) + 1), x1, label='train')
    ax.plot(range(1, len(x2) + 1), x2, label='val')
    ax.set_ylabel(title)
    ax.set_xlabel('Epoch')
    ax.legend()
    ax.margins(0)


def plot_history(history, history_name):
    fig, axes = plt.subplots(ncols=2, nrows=2, figsize=(16, 9))
    ax1, ax2, ax3, ax4 = axes.ravel()
    plot_metrix(ax1, history.history['precision'], history.history['val_precision'], 'Precision')
    plot_metrix(ax2, history.history['recall'], history.history['val_recall'], 'Recall')
    plot_metrix(ax3, history.history['f1'], history.history['val_f1'], "$F_1$")
    plot_metrix(ax4, history.history['loss'], history.history['val_loss'], 'Loss')
    plt.show()
    plt.savefig(history_name)
    plt.close()

# RNN
# plot_history (historyRNN, "rnn_first_history.png")
# predicted = np.round(model.predict(x_test_seq))
# print(classification_report(y_test, predicted, digits=2))
# predicted = np.squeeze(np.asarray(predicted, dtype=int))
# evaluate_prediction(predicted, y_test)
# model.save('models/rnn/rnnmodel_Basic.hdf5')
# os.sys.exit()
#CNN2
# plot_history (historyCNN2, "cnn2_first_history.png")
# predicted = np.round(model.predict(x_test_seq))
# print(classification_report(y_test, predicted, digits=2))
# predicted = np.squeeze(np.asarray(predicted, dtype=int))
# evaluate_prediction(predicted, y_test)
# model.save('models/cnn2/cnnnmodel_Basic.hdf5')
# os.sys.exit()

# CNN
#
# plot_history(history, "first_history.png")
ind_of_film = 2186
print(ind_of_film)

# model = load_model('models/cnn/cnnmodel_Basic.hdf5', custom_objects={'precision' : precision , 'recall' : recall, 'f1' : f1})
predicted = np.round(model.predict(x_test_seq))
print(classification_report(y_test, predicted, digits=2))
model.save('models/cnn/cnnmodel_Basic.hdf5')
print(predicted)
print(x_test)
print(x_test[ind_of_film])
print(y_test[ind_of_film])
pandas = pd.Series([x_test[ind_of_film]]).astype(str)
print(pandas)

x_one_test_seq = get_sequences(tokenizer, pd.Series([x_test[ind_of_film]]).astype(str))
print(x_one_test_seq)
predict_one = model.predict(np.asarray([x_one_test_seq[0]]))
print('predicted = ' + str(np.round(predict_one)))
predicted = np.squeeze(np.asarray(predicted, dtype=int))
evaluate_prediction(predicted, y_test)







# TODO не весь код сюда поместил попробовать доубить при рзаных эпохаха и параметрах
# Дообучаем модель

# model.save_weights('models/cnn/cnnmodel_1.hdf5')
# model.load_weights('models/cnn/cnn-frozen-embeddings-09-0.55.hdf5')
#
# from keras import optimizers
#
# model.layers[1].trainable = True
# adam = optimizers.Adam(lr=0.0001)
# model.compile(loss='binary_crossentropy', optimizer=adam, metrics=[precision, recall, f1])
# model.summary()
#
#
# checkpoint = ModelCheckpoint("models/cnn/cnn-trainable-{epoch:02d}-{val_f1:.2f}.hdf5", monitor='val_f1', save_best_only=True, mode='max', period=1)
#
# history_trainable = model.fit(x_train_seq, y_train, batch_size=32, epochs=5, validation_split=0.25, callbacks = [checkpoint])
#
#
#
# plot_history(history, "second_history.png")
#
#
# model.load_weights('models/cnn/cnn-frozen-embeddings-09-0.55.hdf5')
#
# from sklearn.metrics import classification_report
#
# predicted = np.round(model.predict(x_test_seq))
#
#
# print(classification_report(y_test, predicted, digits=5))





# joblib.dump(logreg, 'classificatorLog.pkl') # после обучения запихиваем обученный в файл
# print(" предсказание")
# mlp = joblib.load('classificatorMLP.pkl')  # классификатор написанный scikitom
# random_forest = joblib.load('classificatorRF.pkl')  # классификатор написанный scikitom
# logreg = joblib.load('classificatorLog.pkl')  # классификатор написанный scikitom
#
# print(mlp.predict(test_doc_average)[0.])
# print(random_forest.predict(test_doc_average)[0])
# print(logreg.predict(test_doc_average)[0])


# print (wv.most_similar(positive=[X_test_word_average[10]], topn=5))


# train_tagged = train_data.apply(lambda r: TaggedDocument(words=tokenize_text(r['ttext']), tags=[r.tag]), axis=1) # пометили документы тэгом
# test_tagged = test_data.apply(lambda  r : TaggedDocument(words= w2v_tokenize_text(r['ttext']), tags = [r.tag]), axis=1)
# train_sent = train_tagged.values
# test_sent = test_tagged.values

#

#doc2vec_model = Doc2Vec(train_sent,size=300, window=10, min_count=5, workers=11,alpha=0.025, min_alpha=0.025)
#train_targets, train_regressors = zip(*[(doc.tags[0], doc2vec_model.infer_vector(doc.words,steps=20)) for doc in train_sent ])
#test_targets, test_regressors = zip(*[(doc.tags[0], doc2vec_model.infer_vector(doc.words,steps=20)) for doc in test_sent ])

#
# print(test_tagged.values[2])

#logreg = linear_model.LogisticRegression(n_jobs=1, C=1e5)
#logreg = logreg.fit(train_regressors,train_targets) #
#predicted = logreg.predict(test_regressors)
#evaluate_prediction(predicted,test_targets)

#print(doc2vec_model.docvecs.most_similar('руководство'))

# print(doc2vec_model.docvecs.most_similar('бухгалтерия'))
#
# print(doc2vec_model.most_similar([doc2vec_model.docvecs['руководство']]))
# print(doc2vec_model.most_similar([doc2vec_model.docvecs['снабжение']]))
# print(doc2vec_model.most_similar([doc2vec_model.docvecs['бухгалтерия']]))
#
# print(doc2vec_model.most_similar([doc2vec_model.docvecs['отдел_кадров']]))


# ind_of_film = 7
# predicted = logreg.predict(X_test_word_average[ind_of_film])
# predicted2 = clf.predict(X_test_word_average[ind_of_film])
# predicted3 = random_forest.predict(X_test_word_average[ind_of_film])
# test_array_plot = np.asarray(test_data['plot'])
# test_tag_array = np.asarray(test_data['tag'])
# print "plot of the film : "
# print (test_data.iloc()[ind_of_film]['plot'])
# print "real genre of the film = " + str(test_tag_array[ind_of_film])
# print "classifier answer of logreg = " + str(predicted)
# print "classifier answer of NLP = " + str(predicted2)
# print "classifier answer of RandomForest = " + str(predicted3)
# print "main ideas of the film"
# print wv.most_similar(positive=[X_test_word_average[ind_of_film]], topn=5)
