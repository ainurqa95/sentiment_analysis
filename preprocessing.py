# -*- coding: utf-8 -*-
 # import modules and set up logging
import sys
from pathlib import Path
import time
import xlrd
from gensim.models import word2vec
import logging
from scipy.interpolate import rbf
logging.root.handlers = []
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from smart_open import smart_open
from sklearn.externals import joblib
import pandas as pd
import numpy as np
from gensim.models import Word2Vec
from numpy import random
import nltk
import pymorphy2
import regex
import gensim
from  itertools import islice
# from sklearn.cross_validation import train_test_split
from sklearn import linear_model
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import accuracy_score, confusion_matrix
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import nltk
import os
# nltk.download('stopwords')
from gensim.models import word2vec
from gensim.models import Doc2Vec
from gensim.models.doc2vec import TaggedDocument
from sklearn.neighbors import KNeighborsClassifier
from nltk.corpus import stopwords
from sklearn import tree
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
import re
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
morph = pymorphy2.MorphAnalyzer()
punct = re.compile('^(.*?)([а-яА-ЯёЁ-]+)(.*?)$')

cotags = {'ADJF':'ADJ', # pymorphy2: word2vec
'ADJS' : 'ADJ',
'ADVB' : 'ADV',
'COMP' : 'ADV',
'GRND' : 'VERB',
'INFN' : 'VERB',
'NOUN' : 'NOUN',
'PRED' : 'ADV',
'PRTF' : 'ADJ',
'PRTS' : 'VERB',
'VERB' : 'VERB',
'INTJ' : 'INTJ',
'PART' : 'PART'
        }


def tokenize_text (text):
    tokens = []
    for sent in nltk.sent_tokenize(text):
        for word in nltk.word_tokenize(sent):
            if len(word)<2 :
                continue
            if word in stopwords.words('russian'):  # deete stop words
                continue
            tokens.append(word.lower())
    return tokens

def plot_confusion_matrix (cm, title='Confusion matrix',cmap=plt.cm.Blues):
    plt.imshow(cm,interpolation='nearest',cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks=np.arange(len(my_tags))
    target_names = my_tags
    plt.xticks(tick_marks,target_names,rotation=45)
    plt.yticks(tick_marks,target_names)
    plt.tight_layout()
    plt.ylabel('Действительность')
    plt.xlabel('Предсказанное')
    plt.savefig('confusion-matrix2.png')
    plt.close()

def evaluate_prediction (predictions,target,title = 'Матрица ошибок'):
    print ('accuracy %s' % str(accuracy_score(target,predictions)))
    cm = confusion_matrix(target,predictions)
    print (' matrix of errors\n %s' % cm)
    print('{row=expected, col=predicted}')
    cm_normalized = cm.astype('float') /cm.sum(axis=1)[:,np.newaxis]
    plot_confusion_matrix(cm_normalized,title+ ' ')

def predict (vectorizer,classifier,data): # predskazat' po nashim dannim
    data_features = vectorizer.transform(data['plot'])
    predictions = classifier.predict(data_features)
    target = data['tag']
    evaluate_prediction(predictions,target)


def w2v_tokenize_text(wv, item_of_dataframe):
    tokens = []
    not_taged_tokens = []
    text = item_of_dataframe['text']
    global arrayOfEmptyColumns
    for sent in nltk.sent_tokenize(text, language='russian'):
        for word in nltk.word_tokenize(sent, language='russian'):
            match = re.search(r"[а-яА-Яё-]+", word)
            if match == None:
                continue
            word = word.lower()
            if word == 'не':
                word = 'отрицание'
            if word in stopwords.words('russian'):# deete stop words
                continue

            parse_result = morph.parse(word)[0]
            word = parse_result.normal_form
            parse_result = morph.parse(word)[0]

            if parse_result.tag.POS not in cotags.keys():
                continue
            not_taged_word = word
            word = word + '_' + cotags[parse_result.tag.POS]  # dobavili chast' rechi
            if word not in wv.vocab:
                continue
            tokens.append(word)
            not_taged_tokens.append(not_taged_word)
    if len(tokens) < 3:
        arrayOfEmptyColumns.append(item_of_dataframe['id'])
    return tokens, not_taged_tokens



def word_averaging(wv, words):

    all_words, mean = set(), []
    for word in words:
        if isinstance(word, np.ndarray):
            mean.append(word)
        elif word in wv.vocab:
            mean.append(wv.syn0norm[wv.vocab[word].index])
            all_words.add(wv.vocab[word].index)

    if not mean:
        logging.warning("cannot compute similarity with no input %s", words)
        # FIXME: remove these examples in pre-processing
        return np.zeros(300)
    mean = gensim.matutils.unitvec(np.array(mean).mean(axis=0)).astype(np.float32)

    return mean #poluchile sredniy vector



def word_averaging_list (wv, text_list):
    return np.vstack([word_averaging(wv,review) for review in text_list])

def cousine_distance (item1, item2):
    return  np.dot(item1, item2) / ( np.linalg.norm(item1) * np.linalg.norm(item2))


arrayOfEmptyColumns = []


# df3 = pd.read_csv('parse_content/result_positive2.csv')
# df3 = pd.read_csv('parse_content/result_new_text.csv')
df3 = pd.read_csv('data/positive_negative.csv')
print(df3['tag'])

# my_tags = ['комедия', 'драма','боевик']
my_tags = [-1, 1]
# for i, row in df3.iterrows(): # меняем разметку
#     if row['rating_review'] == 3:
#         df3.at[i,'tag'] = -1
#     if row['rating_review'] == 4:
#         df3.at[i,'tag'] = 1


df3.tag.value_counts().plot(kind="bar", rot=0)
plt.savefig('data_counts.png')
plt.close()
print(df3['tag'])
print(df3['text'])
# os.sys.exit()
# print(df3['rating_review'])

# os.sys.exit()
#
# train_data,test_data = train_test_split(df3, test_size=0.1,random_state=42)
# test_data.tag.value_counts().plot(kind="bar",rot=0)
# plt.savefig('test_data_counts.png')
# plt.close()
#
# train_data.tag.value_counts().plot(kind="bar",rot=0)
# plt.savefig('train_data_counts.png')
# plt.close()
#
t1 = time.time()

# wv = gensim.models.KeyedVectors.load_word2vec_format('data/tayga_1_2.vec')
# wv = gensim.models.KeyedVectors.load_word2vec_format('data/news_upos_cbow_600_2_2018.vec')
wv = gensim.models.KeyedVectors.load_word2vec_format('data/ruscorpora_upos_skipgram_300_10_2017.bin', binary=True)
wv.init_sims(replace=True)
parse_result = morph.parse('не')[0]
word = parse_result.normal_form
parse_result = morph.parse(word)[0]
print(parse_result)
print(parse_result.tag.POS)

# print(wv.similarity(X_test_word_average[1], wv['нет_NOUN']))

# print(wv['нерекомендовать_VERB'])
# os.sys.exit()

wv.init_sims(replace=True)
print(time.time() - t1)
df = pd.DataFrame(columns=['id','text', 'title' , 'tag'])
for index, row in df3.iterrows():
    taged_tokens, not_taged = w2v_tokenize_text(wv,row)
    df = df.append({'id' : row['id'],'text': taged_tokens, 'title' : " " . join(not_taged), 'tag':row['tag']}, ignore_index=True)
    # df3.at[index, 'text'] = taged_tokens
    # df3.at[index, 'title'] = " " . join(not_taged)
df3 = df

# test_tokenized = df3.apply(lambda r: w2v_tokenize_text(wv,r), axis=1)

# test_tokenized = pd.read_csv('data/test_tokenized.csv')
# train_tokenized = pd.read_csv('data/train_tokenized.csv')
# print(test_tokenized)
# df3.to_csv('parse_content/positive_negative_precomputed0.csv')
# print(arrayOfEmptyColumns)
for item in arrayOfEmptyColumns:
    df3 = df3[df3.id != item]
# print(df3)

df3.to_csv('parse_content/positive_negative_precomputed2.csv')
# df3.to_csv('parse_content/new_texts_precomputed.csv')

# X_train_word_average = word_averaging_list(wv,train_tokenized)
# print(X_train_word_average)
# X_test_word_average = word_averaging_list(wv,test_tokenized)
# print(X_test_word_average)


# tokinezed_test_doc = w2v_tokenize_text(test_doc)
# test_doc_average = word_averaging(wv,tokinezed_test_doc)
# print(test_doc_average)


# print(wv.most_similar(positive=[X_test_word_average[1]], topn=5))

# print(wv['не_рекомендовать_NOUN'])
# print(X_test_word_average[0])


# print(wv.similarity(X_test_word_average[1], wv['снабженец_NOUN']))
# print cosine_similarity = numpy.dot(model['spain'], model['france'])/(numpy.linalg.norm(model['spain'])* numpy.linalg.norm(model['france']))



